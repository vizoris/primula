$(function() {

// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.main-menu').fadeToggle(200);
});


// Випадающее меню
if($(window).width() >= 991){
  $('.drop').hover( function() {
    $(this).children('ul').stop(true,true).fadeToggle()
  })

}

if($(window).width() < 991){
  $('.drop .icon').click( function() {
    $(this).next('ul').stop(true,true).fadeToggle()
  })

}



// Баннер слайдер
$('.banner').slick({
  dots: true,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
});

// Сллайдер команды
$('.team-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});


// Сллайдер докторов
var doctorsSlider = $('.doctors-slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
$('.doctors-slider__next').click(function(){
    $(doctorsSlider).slick("slickNext")
});
$('.doctors-slider__prev').click(function(){
    $(doctorsSlider).slick("slickPrev")
});





// Сллайдер сертификатов
$('.certificate-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});


// FansyBox
 $('.fancybox').fancybox({});











// Акордеон
$(".accordeon-item").click(function(){
    $(this).toggleClass("active");
    $(this).find(".accordeon-item__content").slideToggle(200);
  });


//  Скролл вверх
$('.footer-up').on('click', function(e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: 0
    }, 700);
});


// Кнопка подробнее
$(".doctor-list__more").click(function(){
    $(this).toggleClass("active");
    $(this).parent('.doctor-list').find('.doctor-list__hidden').fadeToggle()
  });


})